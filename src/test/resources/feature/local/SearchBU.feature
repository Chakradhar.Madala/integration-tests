Feature: Searching for Project

Scenario: searching projects with null businessUnitId
Given url 'http://localhost:8080/v1/projects'
When method GET
Then status 200
And match $ contains ({id:1, businessUnitId: null})
And assert response.length == 7


Scenario: searching projects with businessUnitId
Given url 'http://localhost:8080/v1/projects?businessUnitId=1'
When method GET
Then status 200
And match $ == ([])
And assert response.length == 0


